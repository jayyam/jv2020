package java.models;
import java.models.User;
import java.utils.EasyDate;

public class Sessions

	{	
		public enum StateSessions {ACTIVE, CLOSED};
	
		private User user;
		private EasyDate startTime;
		private EasyDate endTime;
		private StateSessions state;
		
		public Sessions (User user)
		
			{
			assert user != null;
		
			this.setUser(user);
			this.startTime=EasyDate.now();
			this.endTime=EasyDate.now();
			this.state = StateSessions.ACTIVE;
		}

public Sessions() 

		{
			this(new User());
		}	

public Sessions(Sessions session) 

		{ 
			this.user = (new User(session.user));
			this.startTime 	= session.startTime.clone();
			this.endTime 	= session.endTime.clone();
			this.state 	= session.state;		
		}
		//Getters		
		public String getId() 
		
			{
				return
						this.user.getId()
						+ "-" +
						this.startTime.toStringTimeStamp();
			}

		public User getUser() 
		{
			return this.user;
		}
		public EasyDate getStartTime()
		{
			return startTime;
		}
		public EasyDate getEndTime()
		{
			return endTime;
		}
		public StateSessions getStatus()
		{
			return state;
		}
		
		//Setters
		public void setUser(User user) 
		{
			this.user = user;
		}
		
		public void setTime(EasyDate endTime)
		{
			this.endTime = endTime;
		}
		public void setStatus(String status)
		{
			this.state = state;
				
		}
		
		//clonar sesion	
		public Sessions clone() 
		{
			return new Sessions(this);
		}
		
		
		//metodo toString
		public String toString()
		
		{
			return String.format("%15s: %-15s\n"
								+"%15s: %-15s\\n"
								+"%15s: %-15s\\n"
								,"User: ", this.user.getName()
								,"startTime: ", this.startTime
								,"endTime: " ,this.endTime
								,"status: ", this.state
								,this.startTime);
		}
		
		
		
		
		//operacion de logueo y deslogueo
		public boolean isLogin()
		{
		 //hay que inventarse un bucle para ejecutar un login desde sesion
		
			do 
			{
				//something	
			}
			while (true);
		}

	//	public void setEndTime(EasyDate now) 
		
		{
			// TODO Auto-generated method stub
			
		}
	
	
	}


