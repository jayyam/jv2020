package java.models;

public interface Identifiable 

	{
		public String getId();
	}
