package java.dataAccess;

import java.models.Mail;
import java.models.Nif;
import java.models.Password;
import java.models.Simulation;
import java.models.User;
import java.models.User.RoleUser;
import java.util.ArrayList;
import java.utils.EasyDate;
import java.models.Sessions;

public class Data
{
	public final static int MAX_DATA = 10; 
	
	private ArrayList<User> usersData;
	private ArrayList<Sessions> sessionsData;
	private ArrayList<Simulation> simulationsData;
	private int registerdUser;
	private int registerdSessions;
	private int registerdSimulations;

	public Data() 
		{		
		this.usersData = new ArrayList<User>();
		this.sessionsData = new ArrayList<Sessions>();
		this.simulationsData = new ArrayList<Simulation>();
		this.registerdUser = 0;
		this.registerdSessions = 0;
		this.registerdSessions = 0;
		loadIntegratedUsers();
		}

	private void loadIntegratedUsers() 
		{
			this.createUser(new User(new Nif("00000000T"),
				"Admin",
				"Admin Admin",
				"La Iglesia, 0, 30012, Patiño",
				new Mail("admin@gmail.com"),
				new EasyDate(2000, 1, 14),
				new EasyDate(2021, 1, 14),
				new Password("Miau#00"), 
				RoleUser.REGISTERED
				));
			this.createUser(new User(new Nif("00000001R"),
				"Guest",
				"Guest Guest",
				"La Iglesia, 0, 30012, Patiño",
				new Mail("guest@gmail.com"),
				new EasyDate(2000, 1, 14),
				new EasyDate(2021, 1, 14),
				new Password("Miau#00"), 
				RoleUser.REGISTERED
				));
		}

	// Users

	public User findUser(String id) 
		{
		for (User user : this.usersData) 
			{
			if (user != null && user.getNif().getText().equals(id))
				{
				return user;
				}
			}
		return null;
	}

	public void createUser(User user)
		{
		if (findUser(user.getNif().getText()) == null) 
			{
			this.usersData.add(user);
			this.registerdUser++;
			return;
			}
		}

	public void updateUser(User user)
		{
		User userOld = findUser(user.getNif().getText());
		if (userOld != null) 
			{
				this.usersData.add(this.indexOfUser(userOld), user);
				return;
			}
		}

	public void deleteUser(String id) {
		User user = findUser(id);

		if (user != null) {
			this.usersData.remove(this.indexOfUser(user));
			this.registerdUser--;
			return;
		}
	}

	private int indexOfUser(User user)
		{
		for (int i=0; i < this.usersData.size(); i++) 
			{
			if (user.equals(this.usersData.get(i))) 
				{
				return i;
				}
		}
		return -1;
	}

	public Sessions findSession(String id)
		{
		for (Sessions session : this.sessionsData)
			{
				if (session != null && session.getId().equals(id)) 
					{
					return session;
					}
			}
		return null;
	}

	public void createSession(Sessions session) {
		this.sessionsData.add(session);
		this.registerdSessions++;
		return;
	}

	public void updateSession(Sessions session) {
		Sessions sessionOld = this.findSession(session.getId());
		if (sessionOld != null) {
			this.sessionsData.add(this.indexOfSession(sessionOld), session);
			return;
		}
	}

	private int indexOfSession(Sessions session)
	
		{
			for (int i=0; i < this.sessionsData.size(); i++)
				{
				if (session.equals(this.sessionsData.get(i)))
					{
						return i;
					}
				}
			return -1;
		}
	
	// Simulations

	public Simulation findSimulation(String id) 
		{
			for (Simulation simulation : this.simulationsData)
				{
				if (simulation != null && simulation.getId().equals(id))
					{
					return simulation;
					}
				}
		return null;
	}

	public void createSimulation(Simulation simulation) 
		{
			if (findUser(simulation.getId()) == null)
				{
					this.simulationsData.add(simulation);
					this.registerdSimulations++;
					return;
				}
		}
	
	public void updateSimulation(Simulation simulation) 
		{
			Simulation simulationOld = this.findSimulation(simulation.getId());
			if (simulationOld != null)
				{
				this.simulationsData.add(this.indexOfSimulation(simulationOld), simulation);
				return;
		}
	}

	private int indexOfSimulation(Simulation simulation) 
		{
			for (int i=0; i < this.simulationsData.size(); i++) 
				{
					if (simulation.equals(this.simulationsData.get(i))) 
						{
						return i;
						}
				}
			return -1;
		}

}
