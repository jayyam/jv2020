package java.models;
/*
 * 						  CRUD
 * 
 * 	;;CREATE=add;--;RETRIEVE=read;--;UPDATE=;--;DELETE=remove;;
 * 
 * 
 * 
*/
import java.util.ArrayList;


public class PruebaArrayLis {

	public static void main(String[] args)
	
		{//programacion parametrizada: especificar el tipo de dato que va a a recibir EL CONSTRUCTOR : en este caso ArrayList
			//ArrayList<TIPODEDATO> listaCompra = newArrayList<TIPODEDATO>();
			ArrayList listaCompra = new ArrayList();
			listaCompra.add("Leche");
			listaCompra.add("Miel");
			listaCompra.add("Aceitunas");
			listaCompra.add("Cerveza");
			listaCompra.remove("Aceitunas");
			listaCompra.add(1, "Fruta");
			listaCompra.add(0, "Queso");
			listaCompra.add(4, "Verduras");

			System.out.format("Los %d elementos de la lista de la compra son:\n",  listaCompra.size());
				
			for (int i = 0; i < listaCompra.size(); i++) 
			
				{
					System.out.format("%s\n", listaCompra.get(i));
				}
			
			System.out.format("¿Hay pan en la lista? %b", listaCompra.contains("Pan"));
		}


	}


